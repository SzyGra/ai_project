from priorityQueue import PriorityQueue
from pizzaGuy import PizzaGuy
from graph import Graph

def pizzaGuyFindPath():
        closedList = []
        current_node = None
        updated = False


# enter the graph size here
        graph_size = 12

        matrix = Graph(graph_size)

# Insert the pizzeria and end place coordinates
        pizzeria = matrix.table[0][0]
        order_place = matrix.table[1][0]

        pizzeria.place_pizzeria(order_place)

        print(matrix)

# if you want to test quickly do our algorithm work just uncomment those 2 lines

        matrix.table[0][1].make_wall()
# matrix.table[1][0].make_wall()
        tab=[]
        tab_ruch=[]
        mr_pizza = PizzaGuy(matrix, pizzeria, order_place,tab)
        tab=mr_pizza.ruch_self()
        for i in tab:
                if(i==1):
                        tab_ruch.append('prawo')
                elif(i==0):
                        tab_ruch.append('gora')
                elif(i==2):
                        tab_ruch.append('dol')
                elif(i==3):
                        tab_ruch.append('lewo')
        return tab_ruch