from manhattanDistance import manhattan_distance
from math import inf, pow, sqrt
import math


class Node:
    def __init__(self, x: int, y: int, weight: int, neighbour_list):
        self.x = x
        self.y = y
        self.weight = weight
        self.neighbour_list = neighbour_list
        self.action_list = ['walk', 'drive', 'fly'] # all possible actions
        # those states are for A*
        self.parent = None
        self.g = inf
        self.h = inf
        self.f = inf

    def __str__(self):
        return str(self.g) + '\t' + str(self.f)

    def place_pizzeria(self, end_node):
        self.parent = self  # pizzeria as a start Node will have parent pointing to itself
        self.g = 0
        self.h = manhattan_distance(self, end_node)
        self.f = manhattan_distance(self, end_node)

    def update(self, current_node, current_direction, end_node):
        g = self.get_distance(current_node)
        if sqrt(pow(self.get_direction(current_node), 2)) == 2:
            g = g * 2
        h = manhattan_distance(self, end_node)
        if (g + h) < self.f:
            self.g = g
            self.h = h
            self.f = g + h
            self.parent = current_node
            return True
        return False
    
    def update_neighbour_list(self, new_neighbours):
        self.neighbour_list = new_neighbours

    def get_distance(self, current_node):
        if self.x == current_node.parent.x and self.y == current_node.y:
            return current_node.g + 2 * self.weight
        else:
            return current_node.g + self.weight
    
    def get_direction(self, current_node):
        if current_node.x > self.x and current_node.y == self.y:
            return 3
        elif current_node.x < self.x and current_node.y == self.y:
            return 1
        elif current_node.x == self.x and current_node.y > self.y:
            return 2
        elif current_node.x == self.x and current_node.y < self.y:
            return 0

    def make_wall(self):
        self.action_list = ['fly']
    
    def make_green(self):
        self.action_list = ['walk', 'fly']
