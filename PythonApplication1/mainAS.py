from priorityQueue import PriorityQueue
from pizzaGuy import PizzaGuy
from graph import Graph

def pizzaGuyFindPath(start_x,start_y,end_x,end_y):
        closedList = []
        current_node = None
        updated = False


# enter the graph size here
        graph_size_x = 11
        graph_size_y = 11

        matrix = Graph(graph_size_x,graph_size_y)

# Insert the pizzeria and end place coordinates
        pizzeria = matrix.table[start_x][start_y]
        order_place = matrix.table[end_x][end_y]

        pizzeria.place_pizzeria(order_place)

        print(matrix)
        matrix.table[9][8].make_wall()
        matrix.table[8][8].make_wall()
        matrix.table[4][2].make_wall()
        matrix.table[4][1].make_wall()
        matrix.table[5][1].make_wall()
        matrix.table[5][2].make_wall()
        matrix.table[6][1].make_wall()
        matrix.table[6][2].make_wall()
        matrix.table[5][5].make_wall()
        matrix.table[5][4].make_wall()
        matrix.table[4][4].make_wall()
        matrix.table[6][4].make_wall()
        matrix.table[4][5].make_wall()
# if you want to test quickly do our algorithm work just uncomment those 2 lines

       
        tab=[]
        tab_ruch=[]
        mr_pizza = PizzaGuy(matrix, pizzeria, order_place,tab)
        tab=mr_pizza.ruch_self()
        for i in tab:
                if(i==1):
                        tab_ruch.append('prawo')
                elif(i==0):
                        tab_ruch.append('dol')
                elif(i==2):
                        tab_ruch.append('gora')
                elif(i==3):
                        tab_ruch.append('lewo')
        return tab_ruch