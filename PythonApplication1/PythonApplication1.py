import pygame
import GeneticAlgorithm as GA
import time
import mainAS as AS
pygame.init()
screen = pygame.display.set_mode((600,600))
screen.fill((34,204,2))
ruch=[]
class pizzaGuyVisual:
        x=0
        y=0
        def __init__(self,car_image_original,car_image_up,car_image_down,car_image_left,car_image_right,x,y,screen,route_image ):
               self.car_image_original=car_image_original
               self.car_image_up=car_image_up
               self.car_image_down=car_image_down
               self.car_image_left=car_image_left
               self.route_image=route_image
               self.car_image_right=car_image_right
               self.screen=screen
               self.x=x
               self.y=y
        def move_x_y(self,movement,max_x_val,max_y_val):
                if(movement=="lewo"):
                             self.car_image_original=self.car_image_left
                             self.x-=40
                             self.printxy()
                             self.screen.blit(self.route_image,(self.x+40,self.y))
                elif(movement=="prawo"):
                             self.car_image_original=self.car_image_right
                             self.x+=40
                             self.printxy()
                             self.screen.blit(self.route_image,(self.x-40,self.y))
                elif(movement=="gora"):
                             self.car_image_original=self.car_image_up
                             self.y-=40
                             self.printxy()
                             self.screen.blit(self.route_image,(self.x,self.y+40))
                elif(movement=="dol"):
                             self.car_image_original=self.car_image_down
                             self.y+=40
                             self.printxy()
                             self.screen.blit(self.route_image,(self.x,self.y-40))
        def printxy(self):
                self.screen.blit(self.car_image_original,(self.x,self.y))


def Houses_Coord(house_number):
        house=[]
        house.clear()
        if(house_number==1):
                house=[1,0]
        if(house_number==2):
                house=[5,0]
        if(house_number==3):
                house=[9,0]
        if(house_number==4):
                house=[1,3]
        if(house_number==5):
                house=[5,3]
        if(house_number==6):
                house=[9,3]
        if(house_number==7):
                house=[1,6]
        if(house_number==8):
                house=[5,6]
        if(house_number==9):
                house=[9,6]
        if(house_number==10):
                house=[1,9]
        if(house_number==11):
                house=[5,9]
        if(house_number==12):
                house=[9,9]
        return house
        
def createHouses(myfont):
        house_x=[]
        house_y=[]
        house_number=0
        road_image=pygame.image.load('D:\\AI_Projekt_Assets\Sprites_2D\\road_grid.png')
        image_house=pygame.image.load('D:\\AI_Projekt_Assets\Sprites_2D\\House.png')
        image_house_big=pygame.transform.scale(image_house,(80,80))
        for k in range(40,520,40):
                for l in range(160,560,40):
                        screen.blit(road_image,(k,l))
                        
        for i in range(80,480,120):
                for j in range(60,480,160):
                        screen.blit(image_house_big,(j,i))
                        house_x.append(j+20)
                        house_y.append(i+80)
                        print(house_x[house_number],house_y[house_number],house_number+1)
                        label = myfont.render((""+str(house_number+1)), 1, (0,0,0))
                        screen.blit(label, (j+30, i+35))
                        house_number=house_number+1

                        
def load_scene():
        
        myfont = pygame.font.SysFont("bold", 35)
        logo=pygame.image.load('D:\\AI_Projekt_Assets\Sprites_2D\\PizzaSlice.png')
        pygame.display.set_icon(logo)
        pygame.display.set_caption('PIZZA GUY')
        print('Wczytywanie i skalowanie tekstur...')
        createHouses(myfont)
            
        
def main():
        run_x=10
        run_y=6
        myfont = pygame.font.SysFont("bold", 35)
        routeForPizzaGuy=GA.genetic_algorithm()
        car_to_create=pygame.image.load('D:\\AI_Projekt_Assets\Sprites_2D\\PizzaGuy.png')
        car_to_create_rescale=pygame.transform.scale(car_to_create,(40,40))
        car_to_create_left=pygame.transform.rotate(car_to_create_rescale,90)
        car_to_create_right=pygame.transform.rotate(car_to_create_rescale,-90)
        car_to_create_down=pygame.transform.rotate(car_to_create_rescale,180)
        road_image=pygame.image.load('D:\\AI_Projekt_Assets\Sprites_2D\\road_grid.png')
        pizzaGuyRun=pizzaGuyVisual(car_to_create_rescale,car_to_create_rescale,car_to_create_down,car_to_create_left,car_to_create_right,440,400,screen,road_image)
        load_scene()
        go_to_location=1
        ar_index=0
        run=True
        ruch=[]
        rond_img=pygame.transform.scale(road_image,(520,40))
        
        while(run):
             
              
              screen.blit(rond_img, (20,20))
              if(go_to_location ==1):
                    if(ar_index<11):
                        move_ar=Houses_Coord(routeForPizzaGuy[ar_index])
                        label = myfont.render(""+str(routeForPizzaGuy[ar_index])+"   "+str(routeForPizzaGuy), 1, (0,0,0))
                        screen.blit(label, (40,20))
                        ruch=AS.pizzaGuyFindPath(run_x,run_y,move_ar[0],move_ar[-1])
                        print(ruch)
                    else:
                        label = myfont.render("Wracam!", 1, (0,0,0))
                        screen.blit(label, (40,20))
                        ruch=AS.pizzaGuyFindPath(run_x,run_y,10,6)
                        print(ruch)
                    for i in ruch:
                                pygame.time.delay(180)
                                pizzaGuyRun.move_x_y(i,600,600)
                                print(pizzaGuyRun.x,pizzaGuyRun.y)
                                pygame.display.flip()
                    if(ar_index<11):
					    routeForPizzaGuy[ar_index]
                        run_x=move_ar[0]
                        run_y=move_ar[-1]
                        move_ar.clear()
                        go_to_location=0
                    else:
                        go_to_location=0
                    pygame.display.update()
              for event in pygame.event.get():
                        if event.type == pygame.QUIT:
                                run = False
                        if event.type == pygame.KEYDOWN:
                                if event.key == pygame.K_SPACE:
                                        go_to_location=1
                                        if(ar_index<11):
                                                ar_index=ar_index+1
                                        print('sdsad')
                                if event.key == pygame.K_RIGHT:
                                        print('sdada')
main()