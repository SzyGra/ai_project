class PriorityQueue:
    def __init__(self):
        self.items = []

    def is_empty(self):
        return not bool(len(self.items))

    def __delete__(self):
        if self.is_empty():
            print('Underflow')
            return None
        else:
            return self.items.pop(0)

    def __add__(self, node):
        print('Node:\n', node)
        if self.is_empty():
            self.items.append(node)
        else:
            i = 0
            contain = False

            while not contain and i < len(self.items):
                if node.f < self.items[i].f:
                    self.items.insert(i, node)
                    contain = True
                else:
                    i += 1

            if not contain:
                self.items.append(node)

            print(self)

    def __str__(self):
        if self.is_empty():
            return 'Queue is empty'
        else:
            line = '['
            for x in range(0, len(self.items)):
                line = line + str(self.items[x]) + '\t'
            line = line + ']'
            return line

    def head(self):
        if self.is_empty():
            print('Queue is empty')
            return None
        else:
            self.items[0]

    def tail(self):
        if self.is_empty():
            print('Queue is empty')
            return None
        else:
            self.items[len(self.items) - 1]

