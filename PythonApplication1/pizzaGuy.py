from priorityQueue import PriorityQueue
# directions = { 0: 'north', 1: 'east', 2: 'south', 3: 'west' }

class PizzaGuy:
  def __init__(self, city, pizzeria, delivery_place,ruch):
    self.direction = 1 # default value
    self.pizzeria = pizzeria # remember where you should go back
    self.node = pizzeria # current node
    self.delivery_place = delivery_place # destination node
    self.action = 'drive' # our pizzaGuy will simply go by car
    self.ruch=ruch
    self.start()
  
  def start(self):
    PQ = PriorityQueue()
    closed_list = []
    current_node = None
    PQ.__add__(self.pizzeria)


    # # our pizza guy is now doing mind training
    # # he is going to the pizzeria in his head to find shortest path

    while not PQ.is_empty() and not current_node == self.delivery_place:
        current_node = PQ.__delete__()
        self.direction = current_node.get_direction(self.node)
        self.node = current_node
        closed_list.append(current_node)
        self.ruch=[]
        for neighbour in current_node.neighbour_list:
            if neighbour not in closed_list and self.action in neighbour.action_list:
                is_updated = neighbour.update(current_node, self.direction, self.delivery_place)
                if is_updated:
                    PQ.__add__(neighbour)
		    
    
    # # The algorithm stopped --> now we an subtract shortest path from matrix

    if not self.node == self.delivery_place:

        #  we can't get to delivery point

        print('There is no way to get there by car')
    else:
        closed_list = []
        while not self.node.parent == self.node:
            closed_list.insert(0, self.node)
            self.node = self.node.parent

        self.node = self.pizzeria

        # now we are in start point and just need to go by chosen path

        for a in closed_list:
            print('Current node: ', self.node)
            print('Next node: ', a)
            print('Change direction: ', a.get_direction(self.node))
            self.ruch.append(a.get_direction(self.node))
            self.direction = a.get_direction(self.node)
            self.node = a

    print('\n\nEND:\t', self.node, '\n\t', self.delivery_place)
  def ruch_self(self):
	   return self.ruch
